﻿using PowercastRFID.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PowercastRFID
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        public AboutWindow()
        {
            InitializeComponent();
            
            Closing += AboutWindow_Closing;
        }

        private void AboutWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _logger.Event("EventType=CloseDialog, Name={0}", this.Name);
        }

        private ILogger _logger = LogManager.GetCurrentLogger();
    }
}
