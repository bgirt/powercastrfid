﻿using Org.LLRP.LTK.LLRPV1;
using PowercastRFID.LLRP;
using PowercastRFID.Logging;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;

namespace PowercastRFID
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Constructor

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            _logger.Event("EventType=ApplicationStarted");
        }

        #endregion

        #region Properties

        public string ReaderAddress
        {
            get { return _readerAddress; }
            set
            {
                if (_readerAddress == value) return;
                _readerAddress = value;
                RaisePropertyChanged("ReaderAddress");
            }
        }

        #endregion

        #region Functions

        private void Initialize()
        {
            _readerAddress = Properties.Settings.Default.ReaderIP;
        }

        /// <summary>
        /// Close the application from the file->exit menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuItemAbout_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow aboutDialog = new AboutWindow();

            _logger.Event("EventType=PopupDialog, Name={0}", aboutDialog.Name);

            aboutDialog.ShowDialog();
        }

        private void button_Open_Click(object sender, RoutedEventArgs e)
        {
            _logger.Event($"EventType=ButtonPressed, Name=");

            // Need to start an action with a callback, disable controls (open button, etc...)
            // Then, on callback, re-enable button, put display a message to the user, change button text
            Task.Factory.StartNew(new Action(() =>
            {
                // Try to open a connection to 
                if (_llrpManager.OpenReaderConnection(_readerAddress))
                {
                    // Save the IP address
                    Properties.Settings.Default.Save();
                }
            }));
        }

        private void MainWindowView_Closing(object sender, CancelEventArgs e)
        {
            _logger.Event("EventType=MainWindowClosing");
            // Terminate the LLRP connection
            // Flush and close the log file
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Member Variables

        private ILogger _logger = LogManager.GetCurrentLogger();

        private LlrpManager _llrpManager = new LlrpManager();
        private string _readerAddress;

        #endregion

        
    }
}
