﻿using Org.LLRP.LTK.LLRPV1;
using PowercastRFID.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowercastRFID.LLRP
{
    public class LlrpManager
    {
        #region Constructor

        //Create a singleton instance of this class?

        #endregion

        #region Functions

        /// <summary>
        /// Open IP connection to RFID reader
        /// </summary>
        /// <param name="ipAddr"></param>
        /// <returns></returns>
        public bool OpenReaderConnection(string ipAddr)
        {
            _logger.Info($"Opening RFID reader IP connection to {ipAddr}");
            
            try
            {
                ENUM_ConnectionAttemptStatusType status;

                bool ret = _reader.Open(ipAddr, 5000, out status);

                if (!ret || status != ENUM_ConnectionAttemptStatusType.Success)
                {
                    _logger.Error("IP connection failed. {0}", status);
                    return false;
                }
            }
            catch(Exception ex)
            {
                _logger.Exception(ex, "Exception caught in LlrpManager.OpenReaderConnection({0})", ipAddr);
                return false;
            }

            _logger.Info("Connected to RFID reader.");

            // Subscribe to reader event notifications and RO access report
            _reader.OnReaderEventNotification += new delegateReaderEventNotification(ReaderEventNotificationHandler);
            _reader.OnRoAccessReportReceived += new delegateRoAccessReport(OnRoAccessReportReceivedHandler);

            return true;
        }

        /// <summary>
        /// Close IP connection to RFID reader
        /// </summary>
        public void CloseReaderConnection()
        {
            // Unsubscribe from reader event notifications
            _reader.OnReaderEventNotification -= new delegateReaderEventNotification(ReaderEventNotificationHandler);
            _reader.OnRoAccessReportReceived -= new delegateRoAccessReport(OnRoAccessReportReceivedHandler);

            _reader.Close();
        }

        private void ReaderEventNotificationHandler(MSG_READER_EVENT_NOTIFICATION msg)
        {
            _logger.Info("OnReaderEventNotification event received.");

            //delegateRoAccessReport del = new delegateRoAccessReport(UpdateROReport);
            //this.Invoke(del, msg);
        }

        private void OnRoAccessReportReceivedHandler(MSG_RO_ACCESS_REPORT msg)
        {
            _logger.Info("OnRoAccessReportReceived event received.");

            //delegateReaderEventNotification del = new delegateReaderEventNotification(UpdateReaderEvent);
            //this.Invoke(del, msg);
        }        

        private void UpdateRoReport(MSG_RO_ACCESS_REPORT msg)
        {
           
        }

        private void UpdateReaderEvent(MSG_READER_EVENT_NOTIFICATION msg)
        {
            
        }

        #endregion

        #region Members

        ILogger _logger = LogManager.GetCurrentLogger();

        private LLRPClient _reader = new LLRPClient();
        private MSG_ERROR_MESSAGE _msgErr;


        #endregion
    }
}
