﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;


namespace PowercastRFID.Logging
{
    public partial class LogManager
    {
        #region Constructor

        private LogManager()
        {
            _logQueue = new ConcurrentQueue<StrongBox<string>>();
            _logMessagePending = new AutoResetEvent(false);
            _logDirectory = Directory.GetCurrentDirectory() + @"\Log\"; // TODO: Get from exe.config or default if doesn't exist
            _logFileName = string.Format("{0}{1}", _logDirectory, "Powercast.log");

            _fileAccessMutex = new Mutex(false, "PowercastLogMutex");

            _logger = new Logger(_logFileName);

            _writeThread = new Thread(new ThreadStart(LogWriteThread)) { Name = "LogWriteThread" };
            _writeThread.IsBackground = true;
            _writeThread.Start();
        }

        #region Singleton Instance

        private static volatile LogManager _instance;
        private static readonly object _syncRoot = new object();

        /// <summary>
        /// LogManager Singleton Accessor
        /// </summary>
        public static LogManager Instance
        {
            get
            {
                if(_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null) _instance = new LogManager();
                    }
                }
                return _instance;
            }
        }

        #endregion

        #endregion

        #region Properties
                
        public string LogFileName { get { return _logFileName; } }

        #endregion

        #region Functions

        public static ILogger GetCurrentLogger()
        {
            return LogManager.Instance._logger;
        }

        public void Write(string logEntry)
        {
            _logQueue.Enqueue(new StrongBox<string>(logEntry));
            _logMessagePending.Set();
        }

        private void LogWriteThread()
        {
            try
            {
                while (!_shuttingDown)
                {
                    WriteActiveLogs();                    
                }
            }
            catch (ThreadAbortException)
            {
                //Thread was aborted
                Thread.ResetAbort();
            }
            catch(Exception ex)
            {
                _logger.Exception(ex, "Exception caught in LogManager WriteThread");
            }
        }

        private void WriteActiveLogs()
        {
            string logEntry;
            StrongBox<string> logEntryWrapper;
            try
            {
                try
                {
                    _fileAccessMutex.WaitOne();
                }
                catch (AbandonedMutexException)
                {
                    //we can get an exception here for 
                    //threadaborted exception but we can 
                    //continue and we have mutex
                }

                if (!Directory.Exists(_logDirectory))
                {
                    Directory.CreateDirectory(_logDirectory);
                }

                using (StreamWriter w = File.AppendText(LogFileName))
                {
                    while (_logQueue.TryDequeue(out logEntryWrapper))
                    {
                        logEntry = logEntryWrapper.Value;
                        logEntryWrapper.Value = null;
                        w.WriteLine(logEntry);
                    }
                }
            }
            catch (UnauthorizedAccessException)
            {
                // This is a recoverable exception, rethrow
                throw;
            }
            catch (Exception ex)
            {
                // This may never be successfully written to the log
                _logger.Exception(ex, "LogManager encountered an error when writing to the log file. Message queue count = {0}.",
                    _logQueue.Count);
            }
            finally
            {
                try
                {
                    _fileAccessMutex.ReleaseMutex();
                }
                catch (Exception) { }
            }
        }

        #endregion

        #region Member Variables

        private Logger _logger;
        private string _logDirectory;
        private string _logFileName;

        private Mutex _fileAccessMutex;

        private Thread _writeThread;
        private ConcurrentQueue<StrongBox<string>> _logQueue;
        private AutoResetEvent _logMessagePending;
        private volatile bool _shuttingDown = false;

        #endregion
    }
}
