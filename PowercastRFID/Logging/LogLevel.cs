﻿
namespace PowercastRFID.Logging
{
    /// <summary>
    /// Logging levels
    /// </summary>
    public enum LogLevel : int
    {
        None = 0,
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Event = 5
    }

}
