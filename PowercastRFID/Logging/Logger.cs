﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace PowercastRFID.Logging
{
    public partial class LogManager
    {
        internal class Logger : ILogger
        {
            #region Constructors

            public Logger(string logFileName)
            {
                LogFileName = logFileName;

                _logFileMutex = new Mutex(false, string.Format("{0}LogMutex", Path.GetFileNameWithoutExtension(LogFileName)));

                // TODO: Get from UI input and/or exe.config
                LogLevel = LogLevel.Debug;
            }

            #endregion

            #region Properties            

            /// <summary>
            /// Current log level
            /// </summary>
            public LogLevel LogLevel { get; set; }

            #endregion

            #region Functions

            private void Write(LogLevel level, Exception exception, string format, params object[] args)
            {
                string message = (args == null) ? format : string.Format(format, args);

                // Replace any delimiters with a different character
                message = message.Replace('|', '¦');

                string logEntry = string.Format("{0}|{1}|{2}|{3}|{4}",
                    DateTime.Now.ToString("yyyy-MM-dd"),
                    DateTime.Now.ToString("HH:mm:ss.fff"),
                    level.ToString().ToUpper(),
                    Thread.CurrentThread.ManagedThreadId,
                    message);

                if(exception != null)
                {
                    logEntry += " (Exception=" + exception.Message.Replace('|', '¦') + ")";
                    logEntry += exception.StackTrace.Replace('|', '¦');
                }

                logEntry = logEntry.Replace("\n", "\n>");

                Console.Write(logEntry);

                Instance.Write(logEntry);
            }

            #endregion

            #region ILogger 

            /// <summary>
            /// Path and name of log file
            /// </summary>
            public string LogFileName { get; private set; }

            public void Debug(string format, params object[] args)
            {
                if(LogLevel <= LogLevel.Debug)
                {
                    Write(LogLevel.Debug, null, format, args);
                }
            }

            public void Info(string format, params object[] args)
            {
                if(LogLevel <= LogLevel.Info)
                {
                    Write(LogLevel.Info, null, format, args);
                }
            }

            public void Warn(string format, params object[] args)
            {
                if(LogLevel<= LogLevel.Warn)
                {
                    Write(LogLevel.Warn, null, format, args);
                }
            }

            public void Error(string format, params object[] args)
            {
                if(LogLevel <= LogLevel.Error)
                {
                    Write(LogLevel.Error, null, format, args);
                }
            }

            public void Event(string format, params object[] args)
            {
                Write(LogLevel.Event, null, format, args);
            }
            
            /// <summary>
            /// Log Error Exception
            /// </summary>
            /// <param name="ex"></param>
            /// <param name="format"></param>
            /// <param name="args"></param>
            public void Exception(Exception ex, string format, params object[] args)
            {
                if(LogLevel <= LogLevel.Error)
                {
                    //if(Shuttingdown)
                    //{
                    //    if(ex is ThreadAbortException)
                    //    {
                    //        DebugException(ex, format, args);
                    //        return;
                    //    }
                    //}

                    Write(LogLevel.Error, ex, format, args);
                }
            }

            #endregion

            #region Member Variables

            private Mutex _logFileMutex;

            #endregion
        }
    }
}
