﻿using System;

namespace PowercastRFID.Logging
{
    public interface ILogger
    {
        void Debug(string format, params object[] args);

        void Info(string format, params object[] args);

        void Warn(string format, params object[] args);

        void Error(string format, params object[] args);

        void Event(string format, params object[] args);

        void Exception(Exception ex, string format, params object[] args);

        LogLevel LogLevel { get; set; }
    }
}
